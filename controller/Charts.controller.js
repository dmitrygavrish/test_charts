sap.ui.define([
  "sap/ui/core/mvc/Controller",
  '../control/ChartJSControl',
  '../control/C3JSControl'
], function (Controller, ChartJSControl, C3JSControl) {
  "use strict";

  return Controller.extend("sap.ui.demo.wt.controller.Charts", {
    onInit : function () {

    },
    onAfterRendering: function() {

    },
    onSelectChange1: function(oEvent) {
      var chart = oEvent.getParameter("selectedItem").getText();
      var panel = this.byId("panel1");
      var newChart;

      panel.destroyContent();

      switch(chart) {
        case "Линейный график":
          newChart = new sap.ui.demo.wt.control.ChartJSControl({
            responsive: true,
            chartType: "Line",
            height: 300,
            data: "{charts>/lineData}"
          });
          break;
        case "Столбчатый график":
          newChart = new sap.ui.demo.wt.control.ChartJSControl({
            responsive: true,
            chartType: "Bar",
            height: 300,
            data: "{charts>/barData}"
          });
          break;
        case "Радиальный график":
          newChart = new sap.ui.demo.wt.control.ChartJSControl({
            responsive: true,
            chartType: "Radar",
            height: 300,
            data: "{charts>/radarData}"
          });
          break;
        case "Полярный график":
          newChart = new sap.ui.demo.wt.control.ChartJSControl({
            responsive: true,
            chartType: "PolarArea",
            height: 300,
            data: "{charts>/polarData}"
          });
          break;
        case "Круговой график":
          newChart = new sap.ui.demo.wt.control.ChartJSControl({
            responsive: true,
            chartType: "Pie",
            height: 300,
            data: "{charts>/pieData}"
          });
          break;
        case "Кольцевой график":
          newChart = new sap.ui.demo.wt.control.ChartJSControl({
            responsive: true,
            chartType: "Doughnut",
            height: 300,
            data: "{charts>/pieData}"
          });
          break;
      }

      panel.insertContent(newChart);
    },
    onSelectChange2: function(oEvent) {
      var chart = oEvent.getParameter("selectedItem").getText();
      var panel = this.byId("panel2");
      var newChart;

      panel.destroyContent();

      switch(chart) {
        case "Линейный график":
          newChart = new sap.ui.demo.wt.control.ChartJSControl({
            responsive: true,
            chartType: "Line",
            height: 300,
            data: "{charts>/lineData}"
          });
          break;
        case "Столбчатый график":
          newChart = new sap.ui.demo.wt.control.ChartJSControl({
            responsive: true,
            chartType: "Bar",
            height: 300,
            data: "{charts>/barData}"
          });
          break;
        case "Радиальный график":
          newChart = new sap.ui.demo.wt.control.ChartJSControl({
            responsive: true,
            chartType: "Radar",
            height: 300,
            data: "{charts>/radarData}"
          });
          break;
        case "Полярный график":
          newChart = new sap.ui.demo.wt.control.ChartJSControl({
            responsive: true,
            chartType: "PolarArea",
            height: 300,
            data: "{charts>/polarData}"
          });
          break;
        case "Круговой график":
          newChart = new sap.ui.demo.wt.control.ChartJSControl({
            responsive: true,
            chartType: "Pie",
            height: 300,
            data: "{charts>/pieData}"
          });
          break;
        case "Кольцевой график":
          newChart = new sap.ui.demo.wt.control.ChartJSControl({
            responsive: true,
            chartType: "Doughnut",
            height: 300,
            data: "{charts>/pieData}"
          });
          break;
      }

      panel.insertContent(newChart);
    }
  });
});
